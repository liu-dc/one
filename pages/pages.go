package pages

import (
	"encoding/json"
	"fmt"
	"gitee.com/liu-dc/one/core/try"
	"os"
)

const (
	Dir    = "./pages"
	Config = "page.json"
)

type Page struct {
	Key    string `json:"key"`
	Name   string `json:"name"`
	Icon   string `json:"icon"`
	Desc   string `json:"desc"`
	Remark string `json:"remark"`
	Hide   bool   `json:"hide"`
	Enable bool   `json:"enable"`
}

func Update(page *Page) error {
	key := page.Key
	filepath := fmt.Sprintf("%s/%s/page.json", Dir, key)
	return try.New().
		SaveJsonFile(filepath, page).Err
}
func List() *[]Page {
	var err error
	var entries []os.DirEntry
	entries, err = os.ReadDir(Dir)
	if err != nil {
		return nil
	}
	var list = make([]Page, 0)
	for _, entry := range entries {
		if entry.IsDir() && entry.Name()[:1] != "." {
			var page = Get(entry.Name())
			if page != nil {
				list = append(list, *page)
			}
		}
	}
	return &list
}

func Get(key string) *Page {
	var page Page
	var mainJsonFile []byte
	var err error
	mainJsonFile, err = os.ReadFile(fmt.Sprintf("%s/%s/%s", Dir, key, Config))
	if err == nil {
		err = json.Unmarshal(mainJsonFile, &page)
		if err != nil {
			return nil
		}
		page.Key = key
	} else {
		page = Page{
			Key:  key,
			Name: key,
		}
	}
	return &page
}
