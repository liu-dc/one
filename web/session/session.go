package session

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/liu-dc/one/cache"
	"gitee.com/liu-dc/one/redis"
	"gitee.com/liu-dc/one/web/user"
	"net/http"
	"strings"
	"time"
)

var ctx = context.Background()

// mem 10秒超时缓存
var mem = cache.New[[]byte](func(key string) ([]byte, error) {
	bytes, err := redis.Redis.Get(ctx, key).Bytes()
	return bytes, err
}, 10)

func GetUserStringArrayAll[T any]() (*[]T, error) {
	var err error
	var keys []string
	var list []T
	keys, err = redis.Redis.Keys(ctx, "one:session:*").Result()
	if err != nil {
		return nil, err
	}
	for _, key := range keys {
		var m T
		var bytes []byte
		bytes, err = mem.Get(key)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(bytes, &m)
		list = append(list, m)
	}
	return &list, err
}
func GetToken(request *http.Request) (string, error) {
	cookie, _ := request.Cookie("O-TOKEN")
	if cookie != nil {
		return cookie.Value, nil
	}
	token := request.Header.Get("O-TOKEN")
	if len(token) > 0 {
		return token, nil
	}
	token = request.URL.Query().Get("O-TOKEN")
	if len(token) > 0 {
		return token, nil
	}
	token = request.URL.Query().Get("access_token")
	if len(token) > 0 {
		return token, nil
	}
	return "", errors.New("invalid token")
}

func GetUser(token string, v any) error {

	uStr, err := mem.Get(fmt.Sprintf("one:session:%s", token))
	if err != nil {
		return errors.New("未登录")
	}
	err = json.Unmarshal(uStr, v)
	if err != nil {
		return err
	}
	return nil
}
func GetUserToByte(request *http.Request) ([]byte, error) {

	token, err := GetToken(request)
	if err != nil {
		return nil, err
	}
	if len(token) == 0 {
		return nil, errors.New("未登录")
	}
	return mem.Get(fmt.Sprintf("one:session:%s", token))
}
func GetUserByRequest(request *http.Request, v any) error {
	token, err := GetToken(request)
	if err != nil {
		return err
	}
	if len(token) > 0 {
		return GetUser(token, v)
	}
	return errors.New("未登录")
}

// ExistUserByRequest 会话存在返回nil,不存在返回error
func ExistUserByRequest(request *http.Request) error {
	token, err := GetToken(request)
	if err != nil {
		return err
	}
	return ExistUser(token)
}

// ExistUser 会话存在返回nil,不存在返回error
func ExistUser(token string) error {
	n, _ := redis.Redis.Exists(ctx, fmt.Sprintf("one:session:%s", token)).Result()
	if n > 0 {
		return nil
	}
	return errors.New("not exists")
}
func SetUserByRequest(request *http.Request, user user.User, expiration time.Duration) error {
	token, err := GetToken(request)
	if err != nil {
		return err
	}
	return SetUser(token, user, expiration)
}
func SetUser(token string, user user.User, expiration time.Duration) error {
	if err := DelUser(user.Id); err != nil {
		return err
	}
	bytes, err := json.Marshal(user)
	if err != nil {
		return err
	}
	err = redis.Redis.Set(ctx, fmt.Sprintf("one:session:%s", token), string(bytes), expiration).Err()
	if err != nil {
		return err
	}
	_, _ = mem.Set(token, bytes)
	return nil
}
func DelUser(token string) error {
	uToken := strings.Split(token, ":")[0]
	keys, err := redis.Redis.Keys(ctx, fmt.Sprintf("one:session:%s:*", uToken)).Result()
	if err != nil {
		return err
	}
	if len(keys) > 0 {
		mem.Del(keys...)
		return redis.Redis.Del(ctx, keys...).Err()
	}
	return nil
}
func DelSession(request *http.Request) error {
	if token, err := GetToken(request); err == nil {
		return DelUser(token)
	}
	return nil
}
func HasLogin(request *http.Request) error {
	if err := ExistUserByRequest(request); err != nil {
		return errors.New("未登录")
	}
	return nil
}
func Equals(key, value string) bool {
	var rValue = redis.Redis.Get(ctx, key).String()
	return rValue == value
}
