package router

import (
	"fmt"
	"github.com/gorilla/mux"
	"log/slog"
	"net/http"
	"reflect"
	"regexp"
	"strings"
)

var Router = mux.NewRouter()
var matchPath = regexp.MustCompile("([A-Z][0-9a-z]*)")
var matchPathBy = regexp.MustCompile("(-By-[a-zA-Z0-9]+)")
var methods = []string{"Get", "Post", "Delete", "Put", "Options"}

type RPK string

// parsePath  如ById 参数路径会生成{id}
func parsePath(structName, methodName string) (string, []string) {
	toPath := func(str string) string {
		str = matchPath.ReplaceAllString(str, "-${1}")
		str = matchPathBy.ReplaceAllString(str, "/{${1}}/")
		str = strings.ReplaceAll(str, "{-By-", "{")
		return str
	}
	structName = toPath(structName)
	for _, method := range methods {
		if strings.ToUpper(methodName) == "ANY" {
			return fmt.Sprintf("/%s", structName), nil
		} else if methodName == method {
			return fmt.Sprintf("/%s", structName), []string{method}
		} else if strings.HasPrefix(methodName, method) {
			name := toPath(methodName[len(method):])
			if strings.TrimSpace(structName) == "" {
				return fmt.Sprintf("/%s", name), nil
			} else {
				return fmt.Sprintf("/%s/%s", structName, name), []string{method}
			}
		}
	}
	if strings.TrimSpace(structName) == "" {
		return fmt.Sprintf("/%s", toPath(methodName)), nil
	} else {
		return fmt.Sprintf("/%s/%s", structName, toPath(methodName)), nil
	}
}

// Controller cwl要路过的包名长度
func Controller(cwl int, controllers ...any) {
	for _, controller := range controllers {
		objValue := reflect.ValueOf(controller)
		objType := reflect.TypeOf(controller)

		var structName = objType.Elem().Name()
		pkgName := objType.Elem().PkgPath()
		basePath := pkgName[cwl:]
		num := objValue.NumMethod()
		for i := 0; i < num; i++ {
			methodValue := objValue.Method(i)
			if methodValue.Type().NumIn() == 2 &&
				methodValue.Type().In(0).String() == "http.ResponseWriter" &&
				methodValue.Type().In(1).String() == "*http.Request" {
				fn := methodValue.Interface().(func(writer http.ResponseWriter, request *http.Request))
				path, method := parsePath(structName, objType.Method(i).Name)
				path = strings.ReplaceAll(path, "//", "/")
				path = strings.ToLower(path)
				path = basePath + path
				path = strings.ReplaceAll(path, "/-", "/")
				path = strings.ReplaceAll(path, "-/", "/")
				if strings.HasSuffix(path, "/") {
					path = path[:len(path)-1]
				}
				if method != nil {
					slog.Info("init controller", "method", method, "path", path)
					Router.HandleFunc(path, fn).Methods(method...)
				} else {
					slog.Info("init controller", "method", "[any]", "path", path)
					Router.HandleFunc(path, fn)
				}
			}
		}
	}
}
