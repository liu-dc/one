package table

import (
	"database/sql"
	"errors"
	"fmt"
	"gitee.com/liu-dc/one/constant"
	"gitee.com/liu-dc/one/core/is"
	"gitee.com/liu-dc/one/core/to"
	"gitee.com/liu-dc/one/core/try"
	"gitee.com/liu-dc/one/data"
	"gitee.com/liu-dc/one/web/result"
	"gitee.com/liu-dc/one/web/user"
	"github.com/gorilla/mux"
	"gorm.io/gorm"
	"log"
	"log/slog"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

func MapToWhereSql(fields map[string]string, or bool) string {
	if fields == nil {
		return ""
	}
	var sqlWhere string
	for key, value := range fields {
		where := toWheresSql(key, value)
		if len(where) > 0 {
			if or {
				sqlWhere = fmt.Sprintf("%s OR  %s", sqlWhere, toWheresSql(key, value))
			} else {
				sqlWhere = fmt.Sprintf("%s AND %s", sqlWhere, toWheresSql(key, value))
			}
		}
	}
	if len(sqlWhere) > 5 {
		sqlWhere = sqlWhere[5:]
		return sqlWhere
	}
	return ""
}
func toWheresSql(key, value string) string {
	ands := strings.Split(value, "&&")
	ors := strings.Split(value, "||")
	if ands != nil && len(ands) > 1 {
		var andSql string
		for _, v := range ands {
			andV := toWhereSql(key, v)
			if len(andV) > 0 {
				andSql = fmt.Sprintf("%s AND %s", andSql, andV)
			}
		}
		andSql = andSql[5:]
		return fmt.Sprintf("(%s)", andSql)
	} else if ors != nil && len(ors) > 1 {
		var orSql string
		for _, v := range ors {
			orV := toWhereSql(key, v)
			if len(orV) > 0 {
				orSql = fmt.Sprintf("%s OR %s", orSql, orV)
			}
		}
		orSql = orSql[4:]
		return fmt.Sprintf("(%s)", orSql)
	}
	where := toWhereSql(key, value)
	if len(where) > 0 {
		return fmt.Sprintf("(%s)", toWhereSql(key, value))
	}
	return ""
}
func toWhereSql(key, value string) string {
	if strings.TrimSpace(key) == "" || strings.TrimSpace(value) == "" {
		return ""
	}
	key = strings.TrimSpace(key)
	value = strings.TrimSpace(value)
	var newKey = strings.ReplaceAll(key, "'", "''")
	var newValue = strings.ReplaceAll(value, "'", "''")
	if value == "" || key == "" {
		return ""
	} else if value == "=" {
		//1 等于空

		return fmt.Sprintf("LENGTH(TRIM(COALESCE(`%s`,''))) = 0", newKey)
	} else if value == "!=" {
		//2 不等于空
		return fmt.Sprintf("LENGTH(TRIM(COALESCE(`%s`,''))) > 0", newKey)
	} else if len(value) > 1 && value[:1] == "=" {
		//3 等于
		return fmt.Sprintf("`%s` = '%s'", newKey, newValue[1:])
	} else if len(value) > 1 && value[:1] == ">" {
		//4 大于
		reg := regexp.MustCompile(`\D`)
		numberValue := reg.ReplaceAllString(newValue[1:], "")
		return fmt.Sprintf("`%s` > %s", newKey, numberValue)
	} else if len(value) > 1 && value[:1] == "<" {
		//5 小于
		reg := regexp.MustCompile(`\D`)
		numberValue := reg.ReplaceAllString(newValue[1:], "")
		return fmt.Sprintf("`%s` < '%s'", newKey, numberValue)
	} else if len(value) > 2 && value[:2] == "!=" {
		//6 不等于
		return fmt.Sprintf("`%s` != '%s'", newKey, newValue[2:])
	} else if len(value) > 2 && value[:2] == "!*" {
		//7 不以*xx结尾
		return fmt.Sprintf("`%s` NOT LIKE '%s'", newKey, "%"+newValue[2:])
	} else if len(value) > 2 && value[:1] == "!" && value[len(value)-1:] == "*" {
		//8 不以xx*开头
		return fmt.Sprintf("`%s` NOT LIKE '%s'", newKey, newValue[1:len(value)-1]+"%")
	} else if len(value) > 1 && value[:1] == "!" {
		//9 不包含
		return fmt.Sprintf("`%s` NOT LIKE '%s'", newKey, "%"+newValue[1:]+"%")
	} else if len(value) > 2 && value[:1] == "*" && value[len(value)-1:] == "*" {
		// 10 包含
		return fmt.Sprintf("`%s` LIKE '%s'", newKey, "%"+newValue[1:len(value)-1]+"%")
	} else if len(value) > 1 && value[len(value)-1:] == "*" {
		//11 前缀
		return fmt.Sprintf("`%s` LIKE '%s'", newKey, newValue[:len(value)-1]+"%")
	} else if len(value) > 1 && value[:1] == "*" {
		//12 后缀
		return fmt.Sprintf("`%s` LIKE '%s'", newKey, "%"+newValue[1:])
	}
	// 默认:包含
	return fmt.Sprintf("`%s` LIKE '%s'", newKey, "%"+newValue+"%")
}
func ParseFields(fields []string) []string {
	for i, field := range fields {
		fields[i] = fmt.Sprintf("`%s`", field)
	}
	return fields
}
func ParseTableName(tableName string) string {
	try.New().FileTextFunc(fmt.Sprintf("view/%s.sql", tableName), func(text string) {
		tableName = fmt.Sprintf("(%s) _t", text)
	})
	return tableName
}
func ParseWhere(filters []string, or bool) string {
	var filterMap = make(map[string]string)
	for _, filter := range filters {
		i := strings.Index(filter, "=")
		k := filter[:i]
		v := filter[i+1:]
		filterMap[k] = v
	}
	return MapToWhereSql(filterMap, or)
}
func ParseQuery(request *http.Request) map[string][]string {
	var query = make(map[string][]string)
	var rawQuery = request.URL.RawQuery
	if rawQuery != "" {
		var params = strings.Split(rawQuery, "&")
		for _, param := range params {
			i := strings.Index(param, "=")
			k := param[:i]
			v := param[i+1:]
			uv, _ := url.QueryUnescape(v)
			if query[k] == nil {
				query[k] = []string{uv}
			} else {
				query[k] = append(query[k], uv)
			}
		}
	}
	return query
}
func ParseOrders(orders []string) string {
	var orderSql string
	for _, order := range orders {
		fieldName := strings.Split(order, "=")
		if len(fieldName) == 2 {
			field := fieldName[0]
			name := fieldName[1]
			if strings.ToUpper(name) == "ASC" || strings.ToUpper(name) == "DESC" {
				orderSql = orderSql + fmt.Sprintf("`%s` %s,", field, name)
			}
		}
	}
	if len(orderSql) > 0 {
		orderSql = orderSql[:len(orderSql)-1]
	}
	return orderSql
}

// CheckPermission 检查权限
func CheckPermission(name, method, tableName string, u *user.User) error {
	//验证权限
	if u == nil {
		//是否登录
		return errors.New("user not login")
	}
	flag := strings.ToUpper(fmt.Sprintf("%s_table_%s_%s", name, method, tableName))
	if is.Contains(u.Flags, flag, constant.Admin) {
		//有权限
		return nil
	}
	return errors.New(fmt.Sprintf("not permission:%s", flag))
}

// GetByTable 高级查询
// field 指定返回字段
// value 只查询一列并返回value列表
// filter 过滤条件 filter=a>1&filter=b=2
// offset 偏移前面多少行(排除)
// limit 分页查询数据 -1为导出 默认为20
// is-total true,不统计total
// order 可以有多个,排序 order=name=desc&order=code=asc
// isColumns  true,当export和no-total为false前提下可以查询出当前表列有序名
// isOr 多过滤条件下是否OR
func (t *Table) GetByTable(writer http.ResponseWriter, request *http.Request) {
	rewriteUri := fmt.Sprintf("%s %s", request.Method, request.URL.RequestURI())
	var err error
	var tableName = ParseTableName(mux.Vars(request)["table"])

	var query = request.URL.Query()
	var q = ParseQuery(request)
	var limit = to.StringToInt(query.Get("limit"), 20)
	if limit > 500 {
		limit = 500
	} else if limit == 0 {
		limit = 5
	}
	var offset = to.StringToInt(query.Get("offset"), 0)
	var isOr = to.StringToBool(query.Get("is-or"))
	var isTotal = to.StringToBool(query.Get("is-total"))
	var isColumns = to.StringToBool(query.Get("is-columns"))
	var value = query.Get("value") //value=field ,如果有value值则field=*不会生效
	var fields = q["field"]
	var orders = q["order"]
	var filters = q["filter"]
	var resValue []string
	var columnNames []string
	var res []map[string]any
	var total int64
	tx := t.DB().Table(tableName)

	querySql := ParseWhere(filters, isOr)
	if is.NotBlank(querySql) {
		//查询条件
		tx.Where(querySql)
	}
	if is.NotBlank(value) {
		//value列表指定字段
		if value[:1] == "@" {
			//带@前缀去重
			value = value[1:]
			tx.Distinct(value)
		}
		tx.Select(value)
	} else if is.NotBlank(fields) {
		//指定字段
		tx.Select(fields)
	}
	try.New().
		TrueFunc(t.IsPermission, func() error {
			var u *user.User
			if t.GetUser != nil {
				u = t.GetUser(request)
			}
			return CheckPermission(t.Name, request.Method, tableName, u)
		}).
		RunEnd(func() bool {
			if t.RewriteFunc != nil {

				rewriteFunc := t.RewriteFunc[rewriteUri]
				if rewriteFunc != nil {
					return rewriteFunc(writer, request)
				}
			}
			return false
		}).
		Then(func() error {
			if limit == -1 {
				//-1表示导出所有	判断是否有导出权限
			} else if isTotal {
				//统计总数
				err = tx.Count(&total).Error
				if err == nil {
					//分页
					tx.Offset(offset).Limit(limit)
					if isColumns {
						//有序的列
						var rows *sql.Rows
						rows, err = tx.Rows()
						if err == nil {
							columnNames, err = rows.Columns()
						}
					}
				}

			}
			if limit == -1 {
				//-1表示导出所有	判断是否有导出权限
			} else {
				tx.Offset(offset).Limit(limit)
			}
			if err == nil {
				//排序
				orderSql := ParseOrders(orders)
				if is.NotBlank(orderSql) {
					ParseOrders(orders)
					tx.Order(orderSql)
				}
				if is.Blank(value) {
					return tx.Find(&res).Error
				} else {
					return tx.Find(&resValue).Error
				}
			}
			return err
		}).
		Then(func() error {
			if isTotal && limit != -1 {
				if is.Blank(value) {
					var newRes = map[string]any{"list": res, "total": total}
					if isColumns {
						newRes["columns"] = columnNames
					}
					return result.New(newRes).WriterJson(writer)
				} else {
					var newRes = map[string]any{"list": resValue, "total": total}
					if isColumns {
						newRes["columns"] = columnNames
					}
					return result.New(newRes).WriterJson(writer)
				}
			} else {
				if is.Blank(value) {
					return result.New(res).WriterJson(writer)
				} else {
					return result.New(resValue).WriterJson(writer)
				}
			}
		}).
		Catch(func(err error) error {
			return result.NewError(err).WriterJson(writer)
		}).
		Exception(func(err error) {
			slog.Error("error", err)
		})
}

// GetByTableById id查询
func (t *Table) GetByTableById(writer http.ResponseWriter, request *http.Request) {
	var tableName = mux.Vars(request)["table"]
	var u = t.GetUser(request)
	var id = mux.Vars(request)["id"]
	var query = ParseQuery(request)
	var fields = query["field"]
	var res map[string]any
	try.New().
		TrueFunc(t.IsPermission, func() error {
			return CheckPermission(t.Name, request.Method, tableName, u)
		}).
		RunEnd(func() bool {
			if t.RewriteFunc != nil {
				rewriteUri := fmt.Sprintf("%s %s", request.Method, request.URL.RequestURI())
				rewriteFunc := t.RewriteFunc[rewriteUri]
				if rewriteFunc != nil {
					return rewriteFunc(writer, request)
				}
			}
			return false
		}).
		Then(func() error {
			if fields != nil {
				return t.DB().Table(tableName).Select(ParseFields(fields)).Where("id=?", id).Scan(&res).Error
			} else {
				return t.DB().Table(tableName).Where("id=?", id).Scan(&res).Error
			}
		}).
		Then(func() error {
			return result.New(res).WriterJson(writer)
		}).
		Catch(func(err error) error {
			return result.NewError(err).WriterJson(writer)
		}).
		Exception(func(err error) {
			log.Println(err)
		})
}

// GetByTableByIdByField 字段查询 GET .../table/id/field
func (t *Table) GetByTableByIdByField(writer http.ResponseWriter, request *http.Request) {
	var tableName = mux.Vars(request)["table"]
	var u = t.GetUser(request)
	var id = mux.Vars(request)["id"]
	var field = mux.Vars(request)["field"]
	var res string
	try.New().
		TrueFunc(t.IsPermission, func() error {
			return CheckPermission(t.Name, request.Method, tableName, u)
		}).
		RunEnd(func() bool {
			if t.RewriteFunc != nil {
				rewriteUri := fmt.Sprintf("%s %s", request.Method, request.URL.RequestURI())
				rewriteFunc := t.RewriteFunc[rewriteUri]
				if rewriteFunc != nil {
					return rewriteFunc(writer, request)
				}
			}
			return false
		}).
		Then(func() error {
			x := t.DB().Table(tableName).Select(fmt.Sprintf("`%s`", field)).Where("id=?", id).Scan(&res)
			return x.Error
		}).
		Then(func() error {
			return result.New(res).WriterJson(writer)
		}).
		Catch(func(err error) error {
			return result.NewError(err).WriterJson(writer)
		}).
		Exception(func(err error) {
			slog.Error("error", err)
		})
}

// DeleteByTable 删除 DELETE .../table   id可以多个
func (t *Table) DeleteByTable(writer http.ResponseWriter, request *http.Request) {
	var tableName = mux.Vars(request)["table"]
	var u = t.GetUser(request)
	var q = ParseQuery(request)
	var ids = q["id"]
	try.New().
		TrueFunc(t.IsPermission, func() error {
			return CheckPermission(t.Name, request.Method, tableName, u)
		}).
		RunEnd(func() bool {
			if t.RewriteFunc != nil {
				rewriteUri := fmt.Sprintf("%s %s", request.Method, request.URL.RequestURI())
				rewriteFunc := t.RewriteFunc[rewriteUri]
				if rewriteFunc != nil {
					return rewriteFunc(writer, request)
				}
			}
			return false
		}).
		True(ids == nil || len(ids) == 0, "id is null").
		Then(func() error {
			return data.NewTable(tableName).Delete(ids)
		}).
		Then(func() error {
			return result.New(nil).WriterJson(writer)
		}).
		Catch(func(err error) error {
			return result.NewError(err).WriterJson(writer)
		}).
		Exception(func(err error) {
			log.Println(err)
		})
}

// PutByTable 更新数据
func (t *Table) PutByTable(writer http.ResponseWriter, request *http.Request) {
	rewriteUri := fmt.Sprintf("%s %s", request.Method, request.URL.RequestURI())
	var tableName = mux.Vars(request)["table"]
	var u = t.GetUser(request)
	var list []map[string]any
	try.New().
		TrueFunc(t.IsPermission, func() error {
			return CheckPermission(t.Name, request.Method, tableName, u)
		}).
		RunEnd(func() bool {
			if t.RewriteFunc != nil {
				rewriteFunc := t.RewriteFunc[rewriteUri]
				if rewriteFunc != nil {
					return rewriteFunc(writer, request)
				}
			}
			return false
		}).
		JsonReaderUnmarshal(request.Body, &list).
		Then(func() error {
			var err error
			if t.RewriteBody != nil {
				fn := t.RewriteBody[rewriteUri]
				if fn != nil {
					list, err = fn(list)
				}
			}
			return err
		}).
		Then(func() error {
			return data.NewTable(tableName).UserId(u.Id).Update(list...)
		}).
		Then(func() error {
			return result.New(nil).WriterJson(writer)
		}).
		Catch(func(err error) error {
			return result.NewError(err).WriterJson(writer)
		}).
		Exception(func(err error) {
			log.Println(err)
		})
}

// PostByTable 创建数据
func (t *Table) PostByTable(writer http.ResponseWriter, request *http.Request) {
	rewriteUri := fmt.Sprintf("%s %s", request.Method, request.URL.RequestURI())
	var tableName = mux.Vars(request)["table"]
	var u = t.GetUser(request)
	var list []map[string]any
	try.New().
		TrueFunc(t.IsPermission, func() error {
			return CheckPermission(t.Name, request.Method, tableName, u)
		}).
		RunEnd(func() bool {
			if t.RewriteFunc != nil {
				rewriteFunc := t.RewriteFunc[rewriteUri]
				if rewriteFunc != nil {
					return rewriteFunc(writer, request)
				}
			}
			return false
		}).
		JsonReaderUnmarshal(request.Body, &list).
		Then(func() error {
			var err error
			if t.RewriteBody != nil {
				fn := t.RewriteBody[rewriteUri]
				if fn != nil {
					list, err = fn(list)
				}
			}
			return err
		}).
		Then(func() error {
			return data.NewTable(tableName).UserId(u.Id).Create(list...)
		}).
		Then(func() error {
			return result.New(nil).WriterJson(writer)
		}).
		Catch(func(err error) error {
			return result.NewError(err).WriterJson(writer)
		}).
		Exception(func(err error) {
			log.Println(err)
		})
}

type Table struct {
	Name         string
	DB           func() *gorm.DB
	IsPermission bool
	GetUser      func(*http.Request) *user.User
	RewriteFunc  map[string]func(http.ResponseWriter, *http.Request) bool
	RewriteBody  map[string]func([]map[string]any) ([]map[string]any, error)
}
