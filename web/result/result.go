package result

import (
	"encoding/json"
	"net/http"
	"time"
)

type Result struct {
	Success bool   `json:"success"`
	Code    int    `json:"code"`
	Message string `json:"message"`
	Data    any    `json:"data,omitempty"`
	Time    string `json:"time"`
}

func New(data any) *Result {
	return &Result{
		Success: true,
		Data:    data,
		Code:    1000,
		Message: "ok",
		Time:    time.Now().Format("2006-01-02 15:04:05"),
	}
}
func NewError(err error) *Result {
	return &Result{
		Success: false,
		Code:    5000,
		Message: err.Error(),
		Time:    time.Now().Format("2006-01-02 15:04:05"),
	}
}
func NewFail(message string) *Result {
	return &Result{
		Success: false,
		Code:    5001,
		Message: message,
		Time:    time.Now().Format("2006-01-02 15:04:05"),
	}
}
func (r *Result) WriterJson(writer http.ResponseWriter) error {
	toJson, err := r.ToJson()
	if err != nil {
		return r.WriterError(err, writer)
	}
	writer.Header().Set("Content-type", "application/json")
	_, err = writer.Write(toJson)
	return err
}
func (*Result) WriterError(err error, writer http.ResponseWriter) error {
	e := Result{
		Success: false,
		Code:    5000,
		Message: err.Error(),
		Time:    time.Now().Format("2006-01-02 15:04:05"),
	}
	writer.WriteHeader(500)
	eJson, err2 := e.ToJson()
	if err2 != nil {
		_, err3 := writer.Write([]byte(err.Error()))
		return err3
	} else {
		writer.Header().Set("Content-type", "application/json")
		_, err4 := writer.Write(eJson)
		return err4
	}
}
func (r *Result) ToJson() ([]byte, error) {
	data, err := json.Marshal(r)
	if err != nil {
		return nil, err
	}
	return data, nil
}
func (r *Result) Response(writer http.ResponseWriter) error {
	data, err := r.ToJson()
	if err != nil {
		errResult := NewError(err)
		errToJson, err2 := errResult.ToJson()
		if err2 == nil {
			_, _ = writer.Write(errToJson)
		} else {
			_, _ = writer.Write([]byte(err.Error()))
			writer.WriteHeader(500)
		}
		return err
	}
	_, err = writer.Write(data)
	return err
}
