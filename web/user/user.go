package user

import (
	"gitee.com/liu-dc/one/core/is"
	"gitee.com/liu-dc/one/data"
	"gitee.com/liu-dc/one/utils"
	"gorm.io/gorm"
	"strings"
	"time"
)

type User struct {
	data.Mode
	Code                  int64     `gorm:"column:code" json:"code"`
	Email                 string    `gorm:"column:email" json:"email"`
	Avatar                string    `gorm:"column:avatar" json:"avatar"`
	Name                  string    `gorm:"column:name" json:"name"`
	Password              string    `gorm:"column:password" json:"-"`
	Enable                bool      `gorm:"column:enable" json:"enable"`
	PermissionGroupStrIds string    `gorm:"column:permission_group_str_ids" json:"-"`
	PermissionGroupIds    []string  `gorm:"-" json:"permission_group_ids"`
	PermissionStrIds      string    `gorm:"column:permission_str_ids" json:"-"`
	PermissionIds         []string  `gorm:"-" json:"permission_ids"`
	Flags                 []string  `gorm:"-" json:"flags"`
	Time                  time.Time `gorm:"-" json:"time"`
	IpAddress             string    `gorm:"-" json:"ip_address"`
	UserAgent             string    `gorm:"-" json:"user_agent"`
	PlatformUserId        string    `gorm:"-" json:"platform_User_Id"`
}

func (u *User) GetPermissionGroupIds() []string {
	return strings.Split(u.PermissionGroupStrIds, ",")
}
func (u *User) GetPermissionIds() []string {
	return strings.Split(u.PermissionStrIds, ",")
}
func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	u.Id = utils.UUID()
	now := time.Now()
	u.CreatedTime = now
	u.UpdatedTime = now
	userId, _ := tx.Get("userId")
	if is.Blank(userId) {
		userId = "00000000000000000000000000000000"
	}
	u.CreatedBy = userId.(string)
	u.UpdatedBy = userId.(string)
	return nil
}
func (u *User) BeforeUpdate(tx *gorm.DB) (err error) {
	now := time.Now()
	u.UpdatedTime = now
	userId, _ := tx.Get("userId")
	if is.Blank(userId) {
		userId = "00000000000000000000000000000000"
	}
	u.UpdatedBy = userId.(string)
	return nil
}
