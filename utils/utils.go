package utils

import (
	"crypto/md5"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"sort"
	"strings"
)

func UUID() string {
	b := make([]byte, 16)
	_, _ = rand.Read(b)
	return fmt.Sprintf("%x%x%x%x%x", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
}
func Md5(text string) string {
	h := md5.New()
	_, _ = io.WriteString(h, text)
	return fmt.Sprintf("%x", h.Sum(nil))
}

func ReaderToStruct(r io.Reader, v any) error {
	b, err := io.ReadAll(r)
	if err != nil {
		return err
	}
	return json.Unmarshal(b, v)
}

// SliceRemoveDuplicates 数组去重复
func SliceRemoveDuplicates(slice []string) []string {
	sort.Strings(slice)
	i := 0
	var j int
	for {
		if i >= len(slice)-1 {
			break
		}
		for j = i + 1; j < len(slice) && slice[i] == slice[j]; j++ {
		}
		slice = append(slice[:i+1], slice[j:]...)
		i++
	}
	return slice
}

func URLQuery(request *http.Request) map[string][]string {
	var query = make(map[string][]string)
	var rawQuery = request.URL.RawQuery
	if rawQuery != "" {
		var params = strings.Split(rawQuery, "&")
		for _, param := range params {
			i := strings.Index(param, "=")
			k := param[:i]
			v := param[i+1:]
			uv, _ := url.QueryUnescape(v)
			if query[k] == nil {
				query[k] = []string{uv}
			} else {
				query[k] = append(query[k], uv)
			}
		}
	}
	return query
}
