package mail

import (
	"bytes"
	"errors"
	"fmt"
	"gopkg.in/gomail.v2"
	"net/smtp"
)

var dialer *gomail.Dialer

type auth struct {
	host     string
	username string
	password string
}

func (a *auth) Start(server *smtp.ServerInfo) (proto string, toServer []byte, err error) {
	if !server.TLS {
		advertised := false
		for _, mechanism := range server.Auth {
			if mechanism == "LOGIN" {
				advertised = true
				break
			}
		}
		if !advertised {
			return "", nil, errors.New("gomail: unencrypted connection")
		}
	}
	//if server.Name != a.host {
	//	return "", nil, errors.New("gomail: wrong host name")
	//}
	return "LOGIN", nil, nil
}
func (a *auth) Next(fromServer []byte, more bool) (toServer []byte, err error) {
	if !more {
		return nil, nil
	}

	switch {
	case bytes.Equal(fromServer, []byte("Username:")):
		return []byte(a.username), nil
	case bytes.Equal(fromServer, []byte("Password:")):
		return []byte(a.password), nil
	default:
		return nil, fmt.Errorf("gomail: unexpected server challenge: %s", fromServer)
	}
}
func Set(host string, port int, username, password string, ssl bool) error {
	if host == "" || port == 0 || username == "" || password == "" {
		return errors.New("invalid config")
	}
	dialer = gomail.NewDialer(host, port, username, password)
	dialer.SSL = ssl
	dialer.Auth = &auth{
		username: username,
		password: password,
		host:     host,
	}
	if dialer == nil {
		return errors.New("invalid dialer")
	}
	return nil
}
func SendText(to, subject, body string) error {
	if dialer == nil {
		return errors.New("invalid dialer")
	}
	message := gomail.NewMessage()
	message.SetHeader("From", dialer.Username)
	message.SetHeader("To", to)
	message.SetHeader("Subject", subject)
	message.SetBody("text/plain", body)
	err := dialer.DialAndSend(message)
	if err != nil {
		return err
	}
	return nil
}
