package cache

import (
	"gitee.com/liu-dc/one/core/is"
	"sync"
	"time"
)

type Data[T any] struct {
	value T     //值
	ctime int64 //创建时间
}

type Cache[T any] struct {
	data       map[string]*Data[T]
	ttl        int64
	expireFunc func(string) (T, error)
}

var mtx sync.Mutex

// New ttl 秒值
func New[T any](expireFunc func(string) (T, error), ttl int64) *Cache[T] {
	return &Cache[T]{
		data:       make(map[string]*Data[T]),
		ttl:        ttl,
		expireFunc: expireFunc,
	}
}
func (cache *Cache[T]) Get(key string) (T, error) {
	var empty T
	data := cache.data[key]
	if is.NotBlank(data) && data.ctime+cache.ttl > time.Now().Unix() {
		return data.value, nil
	} else {
		mtx.Lock()
		defer func() { mtx.Unlock() }()
		data = cache.data[key]
		if is.NotBlank(data) && data.ctime+cache.ttl > time.Now().Unix() {
			return data.value, nil
		} else {
			newValue, err := cache.expireFunc(key)
			if err != nil {
				return empty, err
			}
			if is.NotBlank(newValue) {
				return cache.Set(key, newValue)
			}
			cache.Del(key)
		}
	}
	return empty, nil
}
func (cache *Cache[T]) Set(key string, value T) (T, error) {
	time.Now().Unix()
	cache.data[key] = &Data[T]{
		value: value,
		ctime: time.Now().Unix(),
	}
	return cache.Get(key)
}
func (cache *Cache[T]) Del(keys ...string) {
	for _, key := range keys {
		delete(cache.data, key)
	}
}
func (cache *Cache[T]) Clear() {
	clear(cache.data)
}
