package services

import (
	"encoding/json"
	"fmt"
	"gitee.com/liu-dc/one/core/try"
	"log/slog"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"os/exec"
	"strings"
	"time"
)

const (
	Dir    = "./services"
	Main   = "./main"
	Config = "service.json"
)

type Service struct {
	Main      string `json:"main"`
	Start     bool   `json:"start"` //自动启动
	Name      string `json:"name"`
	Desc      string `json:"desc"`
	Key       string `json:"key"`
	Remark    string `json:"remark"`
	Rerunning bool   `json:"rerunning"` //运行中
	port      int
	cmd       *exec.Cmd
	pid       int
	ppid      int
}

func (s *Service) Port() int {
	return s.port
}

type Services struct {
	list map[string]*Service
}

func Start(dir string) *Services {
	var err error
	services := Services{
		make(map[string]*Service),
	}
	var entries []os.DirEntry
	entries, err = os.ReadDir(dir)
	if err != nil {
		panic(err)
	}
	for _, entry := range entries {
		if entry.IsDir() && entry.Name()[:1] != "." {
			err = services.Start(entry.Name(), false, 0)
			if err != nil {
				fmt.Println(err)
			}
		}
	}
	return &services
}

// Start run是否启动，Start为false也启动。
func (ss *Services) Start(sKey string, run bool, delay time.Duration) error {
	var err error
	var s Service
	var mainJsonFile []byte
	mainJsonFile, err = os.ReadFile(fmt.Sprintf("%s/%s/%s", Dir, sKey, Config))
	if err == nil {
		err = json.Unmarshal(mainJsonFile, &s)
		if err != nil {
			return err
		}
	} else {
		s = Service{Start: true, Desc: "", Name: sKey}
	}
	_ = ss.Kill(sKey)
	s.Rerunning = false
	ss.list[sKey] = &s
	if s.Start == false && run == false {
		return err
	}
	addr, err := GetAddr("127.0.0.1")
	if err != nil {
		return err
	}
	var cmd *exec.Cmd

	if len(s.Main) > 0 {
		cmds := strings.Split(fmt.Sprintf("%s %s %s", s.Main, "-addr", addr.String()), " ")
		cmd = exec.Command(cmds[0], cmds[1:]...)
	} else {
		cmd = exec.Command(Main, "-addr", addr.String())
	}
	cmd.Dir = fmt.Sprintf("%s/%s/", Dir, sKey)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	print(cmd.Env)
	err = cmd.Start()
	if err != nil {
		slog.Error("error", err)
		return err
	}
	t := time.Now()
	slog.Info("start", "sKey", sKey, "pid", cmd.Process.Pid, "ppid", os.Getppid(), "port", addr.Port, "time", t)
	s.Key = sKey
	s.cmd = cmd
	s.port = addr.Port
	s.pid = cmd.Process.Pid
	s.ppid = os.Getppid()
	s.Rerunning = true
	ss.list[sKey] = &s
	go func() {
		if err = cmd.Wait(); err != nil {
			slog.Error("error", err)
		}
		s.Rerunning = false
	}()
	if delay > 0 {
		time.Sleep(delay)
	}
	return err
}
func (ss *Services) Kill(sKey string) error {
	s := ss.list[sKey]
	if s == nil {
		return nil
	}
	defer delete(ss.list, sKey)
	if s.cmd != nil {
		slog.Info("exit", "sKey", sKey, "pid", s.cmd.Process.Pid, "ppid", os.Getppid(), "port", s.Port)
		return s.cmd.Process.Kill()
	}
	return nil
}
func (ss *Services) Get(sKey string) *Service {
	return ss.list[sKey]
}
func (ss *Services) List() *[]Service {
	var err error
	var entries []os.DirEntry
	entries, err = os.ReadDir(Dir)
	if err != nil {
		return nil
	}
	var list = make([]Service, 0)
	for _, entry := range entries {
		if entry.IsDir() && entry.Name()[:1] != "." {
			var s Service
			var mainJsonFile []byte
			var sKey = entry.Name()
			mainJsonFile, err = os.ReadFile(fmt.Sprintf("%s/%s/%s", Dir, sKey, Config))
			if err == nil {
				err = json.Unmarshal(mainJsonFile, &s)
				if err != nil {
					continue
				}
				s.Key = sKey
			} else {
				s = Service{Start: true, Desc: "", Name: sKey, Key: sKey}
			}
			a := ss.Get(sKey)
			if a != nil {
				s.Rerunning = a.Rerunning
			}
			list = append(list, s)
		}
	}
	return &list
}
func (ss *Services) Update(s Service) error {
	sKey := s.Key
	filepath := fmt.Sprintf("%s/%s/%s", Dir, sKey, Config)
	return try.New().
		SaveJsonFile(filepath, &s).
		Then(func() error {
			_sKey := ss.Get(sKey)
			if _sKey != nil {
				if _sKey.Rerunning == true && s.Rerunning == false {
					return ss.Kill(sKey)
				} else if _sKey.Rerunning == false && s.Rerunning == true {
					return ss.Start(sKey, true, time.Duration(3)*time.Second)
				}
			} else if s.Rerunning {
				return ss.Start(sKey, true, time.Duration(3)*time.Second)
			}
			return nil
		}).Err
}
func GetAddr(ip string) (*net.TCPAddr, error) {
	listener, err := net.Listen("tcp", fmt.Sprintf("%s:0", ip))
	if err != nil {
		return nil, err
	}
	defer func() { _ = listener.Close() }()
	return listener.Addr().(*net.TCPAddr), nil
}

// Proxy 反向代理
func (ss *Services) Proxy(name string, writer http.ResponseWriter, request *http.Request) {
	s := ss.Get(name)
	if s == nil {
		writer.WriteHeader(404)
		return
	}
	var err error
	var newUrl *url.URL
	newUrl, err = url.Parse(fmt.Sprintf("http://127.0.0.1:%d", s.port))
	if err != nil {
		writer.WriteHeader(500)
		fmt.Println(err)
		return
	}
	reverseProxy := httputil.NewSingleHostReverseProxy(newUrl)
	reverseProxy.Director = func(request *http.Request) {
		request.URL.Scheme = newUrl.Scheme
		request.URL.Host = newUrl.Host
		request.Host = newUrl.Host
	}
	reverseProxy.ServeHTTP(writer, request)
}
