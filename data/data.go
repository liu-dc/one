package data

import (
	"database/sql"
	"errors"
	"fmt"
	"gitee.com/liu-dc/one/core/is"
	"gitee.com/liu-dc/one/core/try"
	"gitee.com/liu-dc/one/utils"
	"gorm.io/gorm"
	"time"
)

var DB *gorm.DB

func InitDB(dialector gorm.Dialector, opts ...gorm.Option) error {
	if DB != nil {
		db, _ := DB.DB()
		if db != nil {
			_ = db.Close()
		}
	}
	var err error
	try.New().
		Then(func() error {
			DB, err = gorm.Open(dialector, opts...)
			return err
		}).
		Then(func() error {
			var db *sql.DB
			db, err = DB.DB()
			if err != nil {
				return err
			}
			db.SetMaxIdleConns(10)
			db.SetMaxOpenConns(200)
			db.SetConnMaxLifetime(time.Second * 10)
			return db.Ping()
		})
	return err
}
func GetDB() *gorm.DB {
	return DB
}
func ParseTableName(tableName string) string {
	try.New().FileTextFunc(fmt.Sprintf("tables/%s.sql", tableName), func(text string) {
		tableName = fmt.Sprintf("(%s) _t", text)
	})
	return tableName
}
func NewTable(tableName string) *Data {
	return &Data{db: DB, tableName: tableName}
}
func NewTableInDB(db *gorm.DB, tableName string) *Data {
	return &Data{db: db, tableName: tableName}
}
func (data *Data) Update(list ...map[string]any) error {
	if len(list) == 0 {
		return errors.New("list is null")
	}
	var userId = data.userId
	if is.Blank(userId) {
		userId = "00000000000000000000000000000000"
	}
	return data.db.Transaction(func(tx *gorm.DB) error {
		for i, item := range list {
			var idAny = item["id"]
			if is.Blank(idAny) {
				return errors.New(fmt.Sprintf("at %d row,id null", i))
			}
			delete(item, "id")
			var id = idAny.(string)
			item["updated_by"] = userId
			item["updated_time"] = time.Now()
			var err error
			if data.query == nil {
				err = tx.Table(data.tableName).Where("id=?", id).Updates(item).Error
			} else {
				err = tx.Table(data.tableName).Where(data.query, data.args).Updates(item).Error
			}
			if err != nil {
				return err
			}
		}
		return nil
	})

}
func (data *Data) Delete(ids ...any) error {
	if len(ids) > 0 {
		data.query = "id in ?"
		data.args = ids
	}
	if data.query == nil {
		return errors.New("where is null")
	}
	return data.db.Table(data.tableName).Where(data.query, data.args).Delete(nil).Error
}
func (data *Data) Create(list ...map[string]any) error {
	if len(list) == 0 {
		return errors.New("list is null")
	}
	var userId = data.userId
	if is.Blank(userId) {
		userId = "00000000000000000000000000000000"
	}
	var now = time.Now()
	return data.db.Transaction(func(tx *gorm.DB) error {
		for _, item := range list {
			item["id"] = utils.UUID()
			item["updated_by"] = userId
			item["updated_time"] = now
			item["created_by"] = userId
			item["created_time"] = now
			item["updated_by"] = userId
			item["updated_time"] = time.Now()
			err := tx.Table(data.tableName).Create(item).Error
			if err != nil {
				return err
			}
		}
		return nil
	})
}
func (data *Data) UserId(userId string) *Data {
	data.userId = userId
	return data
}
func (data *Data) Where(query interface{}, args ...interface{}) *Data {
	data.query = query
	data.args = args
	return data
}

type Data struct {
	db        *gorm.DB
	tableName string
	userId    string

	query any
	args  []any
}
