package data

import "time"

type Mode struct {
	Id          string    `gorm:"column:id" json:"id"`
	Remark      string    `gorm:"column:remark" json:"remark"`
	CreatedBy   string    `gorm:"column:created_by" json:"created_by"`
	CreatedTime time.Time `gorm:"column:created_time" json:"created_time"`
	UpdatedBy   string    `gorm:"column:updated_by" json:"updated_by"`
	UpdatedTime time.Time `gorm:"column:updated_time" json:"updated_time"`
}
