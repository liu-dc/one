package is

import (
	"os"
	"reflect"
	"regexp"
	"strings"
)

// Null 是否为 nil
func Null(v any) bool {
	return v == nil
}

// Pattern 是否正则校验
func Pattern(v, p string) bool {
	is, err := regexp.MatchString(p, v)
	if err != nil {
		return false
	}
	return is
}

// Length 是否长度范围
func Length(v string, min, max int) bool {
	return len(v) >= min && len(v) <= max
}

// Error 是否错误
func Error(err error) bool {
	return err != nil
}

// True 是否为真
func True(v bool) bool {
	return v
}

// False 是否为假
func False(v bool) bool {
	return v == false
}

// Blank 是否为空白
func Blank(v any) bool {
	switch val := v.(type) {
	case nil:
		return true
	case string:
		return strings.TrimSpace(val) == ""
	case []any:
		return len(val) == 0
	case map[string]any:
		return len(val) == 0
	case int, float32, float64, bool:
		return false
	default:
		return reflect.ValueOf(val).IsZero()
	}
}
func NotBlank(v any) bool {
	return !Blank(v)
}

// FileExist 文件不存在会报错误
func FileExist(filepath string) bool {
	_, err := os.Stat(filepath)
	return !os.IsNotExist(err)
}

// FileNotExist 文件不存在会报错误
func FileNotExist(filepath string) bool {
	_, err := os.Stat(filepath)
	return os.IsNotExist(err)
}

// Contains 判断值是否包含在数组中
func Contains(list []string, args ...string) bool {
	if list == nil || len(list) == 0 {
		return false
	}
	for _, item := range list {
		for _, arg := range args {
			if item == arg {
				return true
			}
		}
	}
	return false
}
