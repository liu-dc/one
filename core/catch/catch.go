package catch

import (
	"archive/zip"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"reflect"
	"strings"
)

// IfElseFunc 如果is真执行trueFn,否则执行falseFn
func IfElseFunc(is bool, trueFn func() error, falseFn func() error) error {
	if is {
		return trueFn()
	} else {
		return falseFn()
	}
}

// IfErrorElseFunc 如果err不为空nil执行errFn,否则执行nilFn
func IfErrorElseFunc(err error, errFn func() error, nilFn func() error) error {
	if err == nil {
		return nilFn()
	} else {
		return errFn()
	}
}

// OrFunc 只要其中一个执行无错即返回nil
func OrFunc(orFn ...func() error) error {
	var lastErr error
	for _, fn := range orFn {
		err := fn()
		if err == nil {
			return nil
		} else {
			lastErr = err
		}
	}
	return lastErr
}

// ErrorFunc 错误执行fn
func ErrorFunc(err error, fn func(err error) error) error {
	if err != nil {
		if fn != nil {
			return fn(err)
		}
	}
	return err
}

// ErrorFuncVoid 错误执行fn
func ErrorFuncVoid(err error, fn func(err error) error) {
	if err != nil {
		if fn != nil {
			fn(err)
		}
	}
}

// ErrorElseFunc 错误执行fn,否则执行elseFn
func ErrorElseFunc(err error, errFn func(err error) error, elseFn func() error) error {
	if err != nil {
		return errFn(err)
	}
	return elseFn()
}

// TrueFunc is为真执行fn
func TrueFunc(is bool, fn func() error) error {
	if is {
		return fn()
	}
	return nil
}

// TrueFuncVoid is为真执行fn 无返回值
func TrueFuncVoid(is bool, fn func()) {
	if is {
		fn()
	}
}

// FalseFunc is为假执行fn
func FalseFunc(is bool, fn func() error) error {
	if !is {
		return fn()
	}
	return nil
}

// FalseFuncVoid is为假执行fn 无返回值
func FalseFuncVoid(is bool, fn func()) {
	if !is {
		fn()
	}
}

// NotBlankFunc v不为空白执行fn
func NotBlankFunc(v any, fn func() error) error {
	if v == nil {
		return nil
	}
	if reflect.TypeOf(v).Kind() == reflect.String {
		if strings.TrimSpace(reflect.ValueOf(v).String()) == "" {
			return nil
		}
	}
	return fn()
}

// NotBlankFuncVoid v不为空白执行fn
func NotBlankFuncVoid(v any, fn func()) {
	if v == nil {
		return
	}
	if reflect.TypeOf(v).Kind() == reflect.String {
		if strings.TrimSpace(reflect.ValueOf(v).String()) == "" {
			return
		}
	}
	fn()
}

// JsonUnmarshal json转对象
func JsonUnmarshal(b []byte, v any) error {
	return json.Unmarshal(b, v)
}

// JsonFileUnmarshal json文件转对象
func JsonFileUnmarshal(filepath string, v any) error {
	var b, err = os.ReadFile(filepath)
	if err != nil {
		return nil
	}
	return JsonUnmarshal(b, v)
}

// SaveFile 输出文件，文件存在会被重写
func SaveFile(filepath string, b []byte) error {
	file, err := os.OpenFile(filepath, os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		return nil
	}
	defer func() {
		_ = file.Close()
	}()
	_, err = file.Write(b)
	return err
}

// SaveJsonFile 输出json文件，文件存在会被重写
func SaveJsonFile(filepath string, v any) error {
	b, err := json.Marshal(v)
	if err != nil {
		return err
	}
	return SaveFile(filepath, b)
}

// FileExist 文件不存在会报错误
func FileExist(filepath string) error {
	_, err := os.Stat(filepath)
	return err
}

// Unzip 解压
func Unzip(files *zip.Reader, outDir string) error {
	var err = os.MkdirAll(outDir, 0755)
	if err != nil {
		return err
	}
	for _, file := range files.File {
		var info = file.FileInfo()
		var header = file.FileHeader
		var name = header.Name
		if info.IsDir() {
			dir := fmt.Sprintf("%s/%s", outDir, name)
			err = os.MkdirAll(dir, 0755)
			if err != nil {
				return err
			}
		} else {
			var temp io.ReadCloser
			temp, err = file.Open()
			if err != nil {
				return err
			}
			var outFile *os.File
			outFile, err = os.Create(fmt.Sprintf("%s/%s", outDir, name))
			if err != nil {
				return err
			}
			_, err = io.Copy(outFile, temp)
			_ = outFile.Close()
			_ = temp.Close()
			if err != nil {
				return err
			}
		}
	}
	return err
}
