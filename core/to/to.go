package to

import (
	"strconv"
	"strings"
)

// StringToInt 字符串转int
func StringToInt(str string, defaultValue int) int {
	i, e := strconv.Atoi(str)
	if e != nil {
		return defaultValue
	}
	return i
}

// StringToBool 字符串转int
func StringToBool(str string) bool {
	return strings.ToLower(strings.TrimSpace(str)) == "true"
}
func String(str, defaultValue string) string {
	if strings.TrimSpace(str) == "" {
		return defaultValue
	}
	return str
}
