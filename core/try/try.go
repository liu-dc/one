// Package try 主要方法
package try

import "gitee.com/liu-dc/one/core/catch"

// Stepper 步骤器调用链
type Stepper struct {
	catch     func(err error) error //错误处理
	Exception func(err error)       //步骤异常
	Err       error                 //步骤错误
	end       bool                  //步骤终止,如果end为true,将不需要关心Err
}

// Exception 异常处理调用链
type Exception struct {
	exception error //步骤异常
	Err       error //错误处理
	end       bool  //步骤终止
}

// Finally 结尾调用链
type Finally struct {
	Err error //错误处理
	end bool  //步骤终止
}

// New 创建步骤器
func New() *Stepper {
	return &Stepper{}
}

// Error 错误异常
func Error(err error) *Stepper {
	var step = &Stepper{}
	return next(step, func() error {
		return err
	})
}

// Then 下一步
func Then(fn func() error) *Stepper {
	var step = &Stepper{}
	return next(step, fn)
}

// NextFunc 是否执行当前步骤的fn,end为true不执行，err不为nil不执行
func next(step *Stepper, fn func() error) *Stepper {
	if step.end || step.Err != nil {
		return step
	}
	step.Err = fn()
	return step
}

// Exec 只管执行,并结束链调用
func (step *Stepper) Exec(fn func()) {
	_ = next(step, func() error {
		fn()
		return nil
	})
}

// CatchEnd 错误处理
func (step *Stepper) CatchEnd(catch func(err error)) {
	_ = step.Catch(func(err error) error {
		catch(err)
		return nil
	})
}

// Catch 错误处理
func (step *Stepper) Catch(catch func(err error) error) *Exception {
	if step.end {
		return &Exception{nil, step.Err, step.end}
	}
	var err = step.Err
	var exception error
	if err != nil {
		if catch != nil {
			exception = catch(err)
		}
		return &Exception{exception, err, step.end}
	}
	return &Exception{exception, err, step.end}
}

// Exception 异常处理
func (p *Exception) Exception(exception func(err error)) *Finally {
	if p.end {
		return &Finally{p.Err, p.end}
	}
	if exception != nil && p.exception != nil {
		exception(p.exception)
	}
	return &Finally{p.Err, p.end}
}

// Finally 结尾处理
func (p *Exception) Finally(finally func(err error)) {
	_ = catch.FalseFunc(p.end, func() error {
		if finally != nil {
			finally(p.Err)
		}
		return nil
	})
}

// Finally 结尾执行
func (f *Finally) Finally(finally func(err error)) {
	catch.TrueFuncVoid(f.end == false && finally != nil, func() {
		finally(f.Err)
	})
}
