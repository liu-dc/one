// Package try 基本步骤
package try

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"errors"
	"gitee.com/liu-dc/one/core/catch"
	"gitee.com/liu-dc/one/core/is"
	"io"
	"net/http"
	"os"
)

func (step *Stepper) TrueEnd(b bool) *Stepper {
	return next(step, func() error {
		if b {
			step.end = true
		}
		return nil
	})
}
func (step *Stepper) TrueFunc(b bool, fn func() error) *Stepper {
	return next(step, func() error {
		if b {
			return fn()
		}
		return nil
	})
}
func (step *Stepper) TrueEndFunc(b bool, fn func() error) *Stepper {
	return next(step, func() error {
		if b {
			step.end = true
		}
		return fn()
	})
}
func (step *Stepper) FalseFunc(b bool, fn func() error) *Stepper {
	return next(step, func() error {
		if b == false {
			return fn()
		}
		return nil
	})
}
func (step *Stepper) FalseEnd(b bool) *Stepper {
	return next(step, func() error {
		if !b {
			step.end = true
		}
		return nil
	})
}
func (step *Stepper) FalseEndFunc(b bool, fn func() error) *Stepper {
	return next(step, func() error {
		if !b {
			step.end = true
		}
		return fn()
	})
}
func (step *Stepper) RequestMethodEnd(request *http.Request, methods ...string) *Stepper {
	return next(step, func() error {
		method := request.Method
		for _, m := range methods {
			if m == method {
				return nil
			}
		}
		step.end = true
		return nil
	})
}

// PostRequestEnd 创建步骤器,如果请求方法是POST，通过否则终止步骤
func (step *Stepper) PostRequestEnd(request *http.Request) *Stepper {
	return next(step, func() error {
		step.end = request.Method != "POST"
		return nil
	})
}

// GetRequestEnd 创建步骤器,如果请求方法是GET，通过否则终止步骤
func (step *Stepper) GetRequestEnd(request *http.Request) *Stepper {
	return next(step, func() error {
		step.end = request.Method != "GET"
		return nil
	})
}

// DeleteRequestEnd 创建步骤器,如果请求方法是DELETE通过，否则终止步骤
func (step *Stepper) DeleteRequestEnd(request *http.Request) *Stepper {
	return next(step, func() error {
		step.end = request.Method != "DELETE"
		return nil
	})
}

// DeletePutEnd 创建步骤器,如果请求方法是PUT通过，否则终止步骤
func (step *Stepper) DeletePutEnd(request *http.Request) *Stepper {
	return next(step, func() error {
		step.end = request.Method != "PUT"
		return nil
	})
}

//-----------------------------------------------------------------check

func (step *Stepper) Pattern(value, pattern, message string) *Stepper {
	return next(step, func() error {
		if is.Pattern(value, pattern) {
			return nil
		}
		return errors.New(message)
	})
}
func (step *Stepper) Length(value string, min, max int, message string) *Stepper {
	return next(step, func() error {
		if is.Length(value, min, max) {
			return nil
		}
		return errors.New(message)
	})
}
func (step *Stepper) Blank(v any, message string) *Stepper {
	return next(step, func() error {
		if is.Blank(v) {
			return errors.New(message)
		}
		return nil
	})
}

// True 为真错误返回message
func (step *Stepper) True(v bool, message string) *Stepper {
	return next(step, func() error {
		if v {
			return errors.New(message)
		}
		return nil
	})
}

// False 为真错误返回message
func (step *Stepper) False(v bool, message string) *Stepper {
	return next(step, func() error {
		if v == false {
			return errors.New(message)
		}
		return nil
	})
}

// -----------------------------------------------------------------check

// DeleteFile 删除文件
func (step *Stepper) DeleteFile(filepath string) *Stepper {
	return next(step, func() error {
		return os.Remove(filepath)
	})
}

// JsonReaderUnmarshal 将Reader转struct
func (step *Stepper) JsonReaderUnmarshal(r io.Reader, v any) *Stepper {
	return next(step, func() error {
		b, err := io.ReadAll(r)
		if err != nil {
			return err
		}
		if b == nil || len(b) == 0 {
			return errors.New("body is null")
		}
		return json.Unmarshal(b, v)
	})
}

// JsonRequestBodyUnmarshal 将Reader转struct 并保留body
func (step *Stepper) JsonRequestBodyUnmarshal(request *http.Request, v any) *Stepper {
	return next(step, func() error {
		r := request.Body
		b, err := io.ReadAll(r)
		if err != nil {
			return err
		}
		if b == nil || len(b) == 0 {
			return errors.New("body is null")
		}
		request.ContentLength = int64(len(b))
		request.Body = io.NopCloser(bytes.NewBuffer(b))
		return json.Unmarshal(b, v)
	})
}

// FileExist 文件不存在报错误
func (step *Stepper) FileExist(filepath string) *Stepper {
	return next(step, func() error {
		return catch.FileExist(filepath)
	})
}

// FileExistFunc 文件不存在报错误
func (step *Stepper) FileExistFunc(filepath string, fn func() error) *Stepper {
	return next(step, func() error {
		if is.FileExist(filepath) {
			return fn()
		}
		return nil
	})
}

// JsonUnmarshal json转对象
func (step *Stepper) JsonUnmarshal(b []byte, v any) *Stepper {
	return next(step, func() error {
		return catch.JsonUnmarshal(b, v)
	})
}

// JsonFileUnmarshal json文件转对象
func (step *Stepper) JsonFileUnmarshal(filepath string, v any) *Stepper {
	return next(step, func() error {
		return catch.JsonFileUnmarshal(filepath, v)
	})
}

// FileTextFunc Text
func (step *Stepper) FileTextFunc(filepath string, fn func(text string)) *Stepper {
	return next(step, func() error {
		var b, err = os.ReadFile(filepath)
		if err != nil {
			return err
		}
		fn(string(b))
		return nil
	})
}

// IfFileTextFunc Text
func (step *Stepper) IfFileTextFunc(filepath string, fn func(text string)) *Stepper {
	return next(step, func() error {
		if is.Blank(filepath) {
			return nil
		}
		var b, err = os.ReadFile(filepath)
		if err != nil {
			return err
		}
		fn(string(b))
		return nil
	})
}

// SaveFile 输出文件，文件存在会被重写
func (step *Stepper) SaveFile(filepath string, b []byte) *Stepper {
	return next(step, func() error {
		return catch.SaveFile(filepath, b)
	})
}

// SaveJsonFile 输出json文件，文件存在会被重写
func (step *Stepper) SaveJsonFile(filepath string, v any) *Stepper {
	return next(step, func() error {
		return catch.SaveJsonFile(filepath, v)
	})
}

// Unzip 解压
func (step *Stepper) Unzip(files *zip.Reader, outDir string) *Stepper {
	return next(step, func() error {
		return catch.Unzip(files, outDir)
	})
}

// BlankFunc 如果为空白执行fn
func (step *Stepper) BlankFunc(v any, fn func() error) *Stepper {
	return next(step, func() error {
		if is.Blank(v) {
			return fn()
		}
		return nil
	})
}

// NotBlankFunc 如果非空白执行fn
func (step *Stepper) NotBlankFunc(v any, fn func() error) *Stepper {
	return next(step, func() error {
		if !is.Blank(v) {
			return fn()
		}
		return nil
	})
}

// BlankEnd 如果为空白终止
func (step *Stepper) BlankEnd(v any) *Stepper {
	return next(step, func() error {
		if is.Blank(v) {
			step.end = true
		}
		return nil
	})
}

// BlankEndFunc 如果为空白终止,否则执行fn
func (step *Stepper) BlankEndFunc(v any, fn func() error) *Stepper {
	return next(step, func() error {
		if is.Blank(v) {
			step.end = true
		}
		return fn()
	})
}

// NotBlankEnd 如果为非空白终止
func (step *Stepper) NotBlankEnd(v any) *Stepper {
	return next(step, func() error {
		if !is.Blank(v) {
			step.end = true
		}
		return nil
	})
}

// NotBlankEndFunc 如果为非空白终止,否则执行fn
func (step *Stepper) NotBlankEndFunc(v any, fn func() error) *Stepper {
	return next(step, func() error {
		if !is.Blank(v) {
			step.end = true
		}
		return fn()
	})
}

// Then 下一步
func (step *Stepper) Then(fn func() error) *Stepper {
	return next(step, fn)
}

// Ok 错误异常
func (step *Stepper) Ok(err error) *Stepper {
	return next(step, func() error {
		return err
	})
}
func (step *Stepper) ErrorElseFunc(err error, errFn func(err error) error, elseFn func() error) *Stepper {
	return next(step, func() error {
		return catch.ErrorElseFunc(err, errFn, elseFn)
	})
}

// IfElseFunc 条件执行trueFn,falseFn
func (step *Stepper) IfElseFunc(is bool, trueFn func() error, falseFn func() error) *Stepper {
	return next(step, func() error {
		return catch.IfElseFunc(is, trueFn, falseFn)
	})
}

// NotBlankFunc 不为空执行fn
//func (step *Stepper) NotBlankFunc(str string, fn func() error) *Stepper {
//	return next(step, func() error {
//		return catch.NotBlankFunc(str, fn)
//	})
//}

// Run 只管执行
func (step *Stepper) Run(fn func()) *Stepper {
	return next(step, func() error {
		fn()
		return nil
	})
}

// RunEnd 返回true结束
func (step *Stepper) RunEnd(fn func() bool) *Stepper {
	return next(step, func() error {
		if fn() {
			step.end = true
		}
		return nil
	})
}
