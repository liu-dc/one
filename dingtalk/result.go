package dingtalk

type Result struct {
	ErrCode      int            `json:"errcode"`
	ErrMsg       string         `json:"errmsg"`
	Result       map[string]any `json:"result"`
	ExpiresIn    int            `json:"expires_in"`
	AccessToken  string         `json:"access_token"`
	RefreshToken int            `json:"refreshToken"`
	Code         string         `json:"code"`
	Message      string         `json:"message"`
}
