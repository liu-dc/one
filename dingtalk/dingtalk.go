package dingtalk

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/liu-dc/one/core/try"
	"log/slog"
	"net/http"
	"strings"
	"time"
)

type Client struct {
	appKey      string
	appSecret   string
	accessToken string
	time        int64
}

func New(appKey string, appSecret string) *Client {
	return &Client{
		appKey:    appKey,
		appSecret: appSecret,
	}
}
func (c *Client) api(uri string) string {
	return fmt.Sprintf("https://api.dingtalk.com%s", uri)
}
func (c *Client) OApi(uri string) string {
	return fmt.Sprintf("https://oapi.dingtalk.com%s", uri)
}
func (c *Client) connect() error {
	var res *http.Response
	var resultToken Result
	return try.Then(func() error {
		var err error
		res, err = http.Get(c.OApi(fmt.Sprintf("/gettoken?appkey=%s&appsecret=%s", c.appKey, c.appSecret)))
		return err
	}).JsonReaderUnmarshal(res.Body, &resultToken).Then(func() error {
		if resultToken.ErrCode == 0 && len(resultToken.AccessToken) > 0 {
			c.time = time.Now().Unix() + int64(resultToken.ExpiresIn-10)
			c.accessToken = resultToken.AccessToken
			return nil
		} else {
			return errors.New(resultToken.ErrMsg)
		}
	}).Err
}
func (c *Client) Login(authCode string) (map[string]any, error) {
	var err error
	var accessToken string
	var unionid string
	var userid string
	var userinfo map[string]any
	try.Then(func() error {
		//accessToken
		var res = struct {
			RequestId    string `json:"requestid"`
			Message      string `json:"message"`
			Code         string `json:"code"`
			AccessToken  string `json:"accessToken"`
			RefreshToken string `json:"refreshToken"`
			ExpireIn     string `json:"expireIn"`
			CorpId       string `json:"corpId"`
		}{}
		err = c.Post(c.api("/v1.0/oauth2/userAccessToken"),
			map[string]any{"clientId": c.appKey, "clientSecret": c.appSecret, "code": authCode, "refreshToken": c.accessToken, "grantType": "authorization_code"},
			&res,
		)
		if err != nil {
			return err
		}
		accessToken = res.AccessToken
		return err
	}).Then(func() error {
		//unionid
		var res map[string]any
		err = c.Get(c.api("/v1.0/contact/users/me"), &res, map[string]string{
			"x-acs-dingtalk-access-token": accessToken,
		})
		if err != nil {
			return err
		}
		//获取unionId
		unionid = res["unionId"].(string)
		return err
	}).Then(func() error {
		//userid
		var res map[string]any
		err = c.Post(c.OApi("/topapi/user/getbyunionid"), map[string]any{"unionid": unionid}, &res)
		if err != nil {
			return err
		}
		var result = res["result"].(map[string]any)
		userid = result["userid"].(string)
		return err
	}).Then(func() error {
		//获得用户详细信息
		var res map[string]any
		err = c.Post(c.OApi("/topapi/v2/user/get"), map[string]any{"userid": userid}, &res)
		if err != nil {
			return err
		}
		userinfo = res["result"].(map[string]any)
		return err
	})
	return userinfo, err
}

// GetAccessToken 获取Token
func (c *Client) GetAccessToken() (string, error) {
	if time.Now().Unix() > c.time {
		slog.Info("应用Token过期,刷新中...")
		err := c.connect()
		if err != nil {
			return "", err
		}
		slog.Info("应用Token过期,刷新完成")
	}
	return c.accessToken, nil
}

func (c *Client) Post(uri string, body map[string]any, t any, header ...map[string]string) error {
	body["language"] = "zh_CN"
	token, err := c.GetAccessToken()
	if err != nil {
		return err
	}
	var bodyByte []byte
	var req *http.Request
	var newUri string
	if strings.Index(uri, "?") > 0 {
		newUri = fmt.Sprintf("%s&access_token=%s", uri, token)
	} else {
		newUri = fmt.Sprintf("%s?access_token=%s", uri, token)
	}
	try.Then(func() error {
		bodyByte, err = json.Marshal(body)
		return err
	}).Then(func() error {
		req, err = http.NewRequest("POST", newUri, strings.NewReader(string(bodyByte)))
		return err
	}).Then(func() error {
		for _, items := range header {
			for k, v := range items {
				req.Header.Set(k, v)
			}
		}
		return request(req, t)
	})
	return err
}
func (c *Client) Get(uri string, t any, header ...map[string]string) error {
	var req *http.Request
	var err error
	var token string
	var newUri string
	if strings.Index(uri, "?") > 0 {
		newUri = fmt.Sprintf("%s&access_token=%s", uri, token)
	} else {
		newUri = fmt.Sprintf("%s?access_token=%s", uri, token)
	}
	token, err = c.GetAccessToken()
	if err != nil {
		return err
	}
	try.Then(func() error {
		req, err = http.NewRequest("GET", newUri, nil)
		return err
	}).Then(func() error {
		//req.Header.Set("x-acs-dingtalk-access-token", token)
		for _, items := range header {
			for k, v := range items {
				req.Header.Set(k, v)
			}
		}
		return request(req, t)
	})
	return err
}
func request(request *http.Request, t any) error {
	var err error
	var res *http.Response
	err = try.
		Then(func() error {
			client := &http.Client{}
			request.Header.Set("Content-Type", "application/json;charset=UTF-8")
			res, err = client.Do(request)
			return err
		}).
		False(res.StatusCode == 200, res.Status).
		JsonReaderUnmarshal(res.Body, &t).Err
	return err
}
