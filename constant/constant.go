package constant

const Admin = "administrator" // 管理员标识
const (
	MainGetSession    = "main_get_session"
	MainDeleteSession = "main_delete_session"
	MainSettingsLogin = "main_settings_login"
)
