package redis

import (
	"context"
	"gitee.com/liu-dc/one/core/try"
	"github.com/go-redis/redis/v8"
)

var Redis *redis.Client

func InitRedis(addr, password string, dbNumber int) error {
	if Redis != nil {
		_ = Redis.Close()
	}
	return try.New().
		Then(func() error {
			Redis = redis.NewClient(&redis.Options{
				Addr:     addr,
				Password: password,
				DB:       dbNumber,
			})
			return Redis.Ping(context.Background()).Err()
		}).Err
}
